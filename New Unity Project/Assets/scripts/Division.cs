﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Division : MonoBehaviour
{
    public int numberSeven;
    public int numberEight;
    public int quotient;
    public int halfHealth;
    public int maxHealth;

    // Start is called before the first frame update
    void Start()
    {
        DivideNumbers();
        DivideArguments();
        DivideHealth();
    }

    public void DivideNumbers()
    {
        quotient = numberSeven / numberEight;

        Debug.Log(quotient);
        Debug.Log(message: "" + (numberSeven / numberEight));
    }

    public void DivideArguments()
    {
        quotient = numberSeven / numberEight;

        Debug.Log(quotient);
        Debug.Log(message: "" + (numberSeven / numberEight));
    }

    public int DivideHealth()
    {
        int playerHealth;

        playerHealth = halfHealth / maxHealth;

        Debug.Log(message: "" + (halfHealth / maxHealth));

        return playerHealth;
    }
}
