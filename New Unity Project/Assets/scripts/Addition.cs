﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Addition : MonoBehaviour
    {
        public int numberOne;
        public int numberTwo;
        public int sum;
        public int halfHealth;
        public int maxHealth;

        // Start is called before the first frame update
        void Start()
        {
            AddNumbers();
            AddArguments();
            AddHealth();
        }

        public void AddNumbers()
        {
            sum = numberOne + numberTwo;

            Debug.Log(sum);
            Debug.Log(message: "" + (numberOne + numberTwo));
        }

        public void AddArguments()
        {
            sum = numberOne + numberTwo;

            Debug.Log(sum);
            Debug.Log(message: "" + (numberOne + numberTwo));
        }

        public int AddHealth()
        {
            int playerHealth;

            playerHealth = halfHealth + maxHealth;

            Debug.Log(message: "" + (halfHealth + maxHealth));

            return playerHealth;
        }
    }
}