﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Subtraction : MonoBehaviour
{
    public int numberThree;
    public int numberFour;
    public int difference;
    public int halfHealth;
    public int maxHealth;

    // Start is called before the first frame update
    void Start()
    {
        SubtractNumbers();
        SubtractArguments();
        SubtractHealth();
    }

    public void SubtractNumbers()
    {
        difference = numberThree - numberFour;

        Debug.Log(difference);
        Debug.Log(message: "" + (numberThree - numberFour));
    }

    public void SubtractArguments()
    {
        difference = numberThree - numberFour;

        Debug.Log(difference);
        Debug.Log(message: "" + (numberThree - numberFour));
    }

    public int SubtractHealth()
    {
        int playerHealth;

        playerHealth = halfHealth - maxHealth;

        Debug.Log(message: "" + (halfHealth - maxHealth));

        return playerHealth;
    }
}
